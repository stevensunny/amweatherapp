//
//  AMAppDelegate.h
//  WeatherApp
//
//  Created by Steven Sunny on 5/02/14.
//  Copyright (c) 2014 AndMine. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AMAppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;

@end
