//
//  AMMainViewController.m
//  WeatherApp
//
//  Created by Steven Sunny on 5/02/14.
//  Copyright (c) 2014 AndMine. All rights reserved.
//

#import "AMMainViewController.h"
#import "AMAPIManager.h"
#import "AFNetworkActivityIndicatorManager.h"

@interface AMMainViewController ()

@property NSString *city;

@end

@implementation AMMainViewController

- (IBAction)buttonReloadClicked:(UIBarButtonItem *)sender {
    [self populateViewWithWeatherData];
}

/*
 * Populate view with weather data using AMAPIManager (singleton)
 * Created by Steven Sunny
 */
- (void) populateViewWithWeatherData
{
    
    /**
     * HTTP Request using AMAPIManager
     * PS: SET THE API URL IN THE SINGLETON CLASS 
     **/
    
    // Make a HTTP request and get the response
    [[AMAPIManager sharedManager] GET:@"" parameters:nil success:^(AFHTTPRequestOperation *operation, id responseObject) {
        // Success block
        NSString *city = [responseObject objectForKey:@"name"]; /* Capture 'city' value from response object */
        
        self.city = city;
        
        /* Iterate over nested JSON Result : http://stackoverflow.com/questions/13473811/afnetworking-and-json
        NSArray *weather = [myAFNetworking.responseObj objectForKey:@"weather"];
        [weather enumerateObjectsUsingBlock:^(id obj, NSUInteger idx, BOOL *stop) {
            NSString *desc = [obj objectForKey:@"description"];
            self.city = desc;
        }];
        */
        
        [self.tableView setHidden:NO];
        [self.tableView reloadData];
        
        NSLog(@"JSON");
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        // Failure block
    }];
    
}

- (id)initWithStyle:(UITableViewStyle)style
{
    self = [super initWithStyle:style];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    [self populateViewWithWeatherData];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    // Return the number of sections.
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    // Return the number of rows in the section.
    return 1;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *CellIdentifier = @"weatherCell";
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier forIndexPath:indexPath];
    
    // Configure the cell...
    cell.textLabel.text = self.city;
    return cell;
}

/*
// Override to support conditional editing of the table view.
- (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath
{
    // Return NO if you do not want the specified item to be editable.
    return YES;
}
*/

/*
// Override to support editing the table view.
- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (editingStyle == UITableViewCellEditingStyleDelete) {
        // Delete the row from the data source
        [tableView deleteRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationFade];
    }   
    else if (editingStyle == UITableViewCellEditingStyleInsert) {
        // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
    }   
}
*/

/*
// Override to support rearranging the table view.
- (void)tableView:(UITableView *)tableView moveRowAtIndexPath:(NSIndexPath *)fromIndexPath toIndexPath:(NSIndexPath *)toIndexPath
{
}
*/

/*
// Override to support conditional rearranging of the table view.
- (BOOL)tableView:(UITableView *)tableView canMoveRowAtIndexPath:(NSIndexPath *)indexPath
{
    // Return NO if you do not want the item to be re-orderable.
    return YES;
}
*/

/*
#pragma mark - Navigation

// In a story board-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}

 */

@end
