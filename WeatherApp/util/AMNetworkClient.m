//
//  AMNetworkClient.m
//  WeatherApp
//
//  Created by Steven Sunny on 26/02/14.
//  Copyright (c) 2014 AndMine. All rights reserved.
//

#import "AMNetworkClient.h"
#import "AFNetworkActivityIndicatorManager.h"

@implementation AMAPIManager

#pragma mark - Singleton

+(instancetype)client
{
    static AMNetworkClient * requests = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        requests = [[AMNetworkClient alloc] initWithBaseURL:[NSURL URLWithString:@""]];
    });
    return requests;
}

#pragma mark - Custom initialization

-(id)initWithBaseURL:(NSURL *)url
{
    self = [super initWithBaseURL:url];
    if(!self)
    {
        return nil;
    }
    
    [[AFNetworkActivityIndicatorManager client] setEnabled:YES];
    
    return self;
}

#pragma mark - Custom fetching functions

-(void)fetchResponseObj:(NSArray *)params completion:(FetchedObject)fetchedObjectBlock
{
    [self GET:@"" parameters:nil success:^(NSURLSessionDataTask *task, id responseObject) {
        fetchedObjectBlock(responseObject);
    } failure:^(NSURLSessionDataTask *task, NSError *error) {
        fetchedObjectBlock([NSArray new]);
        NSLog(@"Error: %@", error);
    }];
}

@end
