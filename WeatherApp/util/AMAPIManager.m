//
//  AMAPIManager.m
//  WeatherApp
//
//  Singleton Class to deal with HTTP Request
//
//  Created by Steven Sunny on 27/02/14.
//  Copyright (c) 2014 AndMine. All rights reserved.
//

#import "AMAPIManager.h"
#import "AFNetworkActivityIndicatorManager.h"

@implementation AMAPIManager

#pragma mark - Initialization

+ (AMAPIManager *)sharedManager
{
    // SET API URL HERE
    static NSString *apiURL = @"http://api.openweathermap.org/data/2.5/weather?q=Melbourne,AU";
    static dispatch_once_t onceToken;
    static AMAPIManager *_sharedManager = nil;

    dispatch_once(&onceToken, ^{
        _sharedManager = [[self alloc]initWithBaseURL:
                      [NSURL URLWithString: apiURL]];
    });
    return _sharedManager;
}


- (id)initWithBaseURL:(NSURL *)url
{
    self = [super initWithBaseURL:url];
    if(!self)
        return nil;
    
    self.requestSerializer = [AFJSONRequestSerializer serializer];
    
    [[AFNetworkActivityIndicatorManager sharedManager] setEnabled: YES];
    
    return self;
}

@end
