//
//  AMAPIManager.h
//  WeatherApp
//
//  Created by Steven Sunny on 27/02/14.
//  Copyright (c) 2014 AndMine. All rights reserved.
//

#import "AFHTTPRequestOperationManager.h"

@interface AMAPIManager : AFHTTPRequestOperationManager

+ (AMAPIManager *)sharedManager;

@end