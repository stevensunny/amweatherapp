//
//  AMNetworkClient.h
//  WeatherApp
//
//  Created by Steven Sunny on 26/02/14.
//  Copyright (c) 2014 AndMine. All rights reserved.
//

#import "AFHTTPSessionManager.h"

typedef void(FetchedObject) (NSArray*object);

@interface AMNetworkClient : AFHTTPSessionManager

+(instancetype)client;

-(void)fetchResponseObj:(NSArray *)params completion:(FetchedObject)fetchedObjectBlock;

@end
